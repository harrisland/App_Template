/**
  ******************************************************************************
  * @file    C18_cfg.h
  * @author  Harris Chen
  * @version V0.0.1
  * @date    22-Sep-2017
  * @brief   This file contains definitions for C18 configs and resources.
  ******************************************************************************  
  */ 
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __C18_CFG_H
#define __C18_CFG_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stm32f2xx.h>
   
/** @addtogroup Utilities
  * @{
  */
typedef enum 
{
  COM1 = 0,
  COM2 = 1
} COM_TypeDef;

typedef struct 
{
  uint8_t ActiveSect;
	uint8_t InactiveSect;
	uint8_t Sect1Valid;
	uint8_t Sect2Valid;
	uint32_t Sect1Ver;
	uint32_t Sect2Ver;
} stBootConfig;

/** @addtogroup C18_LOW_LEVEL_COM
  * @{
  */
#define COMn                             1

/**
 * @brief Definition for COM port1, connected to USART3
 */ 
#define EVAL_COM1                        USART1
#define EVAL_COM1_CLK                    RCC_APB2Periph_USART1
#define EVAL_COM1_TX_PIN                 GPIO_Pin_9
#define EVAL_COM1_TX_GPIO_PORT           GPIOA
#define EVAL_COM1_TX_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_COM1_TX_SOURCE              GPIO_PinSource9
#define EVAL_COM1_TX_AF                  GPIO_AF_USART1
#define EVAL_COM1_RX_PIN                 GPIO_Pin_10
#define EVAL_COM1_RX_GPIO_PORT           GPIOA
#define EVAL_COM1_RX_GPIO_CLK            RCC_APB2Periph_USART1
#define EVAL_COM1_RX_SOURCE              GPIO_PinSource10
#define EVAL_COM1_RX_AF                  GPIO_AF_USART1
#define EVAL_COM1_IRQn                   USART1_IRQn

/**
  * @}
  */ 

#define ACTIVE_SECTION_ADDR							 0x0800D000
#define INACTIVE_SECTION_ADDR						 0x0800D001
#define SECTION_1_VALID_ADDR						 0x0800D002
#define SECTION_2_VALID_ADDR						 0x0800D003
#define SECTION_1_VERSION_ADDR					 0x0800D004
#define SECTION_2_VERSION_ADDR					 0x0800D008
#define SECTION_1												 0x01
#define SECTION_2												 0x02
#define SECTION_VALID										 0xAA
#define SECTION_INVALID									 0x55
/* Section 1 for 0x0801 0000~0x0802 0000 */
#define ADDR_SECTION1   (uint32_t)0x08010000
/* Section 2 for 0x0802 0000~0x0803 0000 */
#define ADDR_SECTION2   (uint32_t)0x08020000
/* Bootconfig for 0x0800 D000~0x0801 0000 */
#define ADDR_BOOTCFG   (uint32_t)0x0800D000
#define ADDR_BOOTLOADER   (uint32_t)0x08000000

/* Time constant for the delay caclulation allowing to have a millisecond 
   incrementing counter. This value should be equal to (System Clock / 1000).
   ie. if system clock = 120MHz then sEE_TIME_CONST should be 120. */
#define sEE_TIME_CONST                   120 
/**
  * @}
  */  
  
/** @defgroup C18_LOW_LEVEL_Exported_Macros
  * @{
  */  
/**
  * @}
  */ 


/** @defgroup C18_LOW_LEVEL_Exported_Functions
  * @{
  */

void STM_EVAL_COMInit(COM_TypeDef COM, USART_InitTypeDef* USART_InitStruct); 
int8_t GetBootConfig (stBootConfig * pBootCfg);
/**
  * @}
  */
  
#ifdef __cplusplus
}
#endif

#endif /* __C18_CFG_H */
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */  

/*****END OF FILE****/
