/**
  ******************************************************************************
  * @file    C18_cfg.c
  * @author  Harris Chen
  * @version V0.0.1
  * @date    20-Sep-2017
  * @brief   This file provides sets of init function
  ******************************************************************************
  */ 
  
/* Includes ------------------------------------------------------------------*/
#include "C18_cfg.h"

/** @addtogroup Utilities
  * @{
  */ 
/**
  * @}
  */ 
	
/** @addtogroup STM32_EVAL
  * @{
  */ 
/**
  * @}
  */ 
	
/** @addtogroup STM322xG_EVAL
  * @{
  */   
/**
  * @}
  */ 
	
/** @defgroup STM322xG_EVAL_LOW_LEVEL 
  * @brief This file provides firmware functions to manage Leds, push-buttons, 
  *        COM ports, SD card on SDIO and serial EEPROM (sEE) available on 
  *        STM322xG-EVAL evaluation board from STMicroelectronics.
  * @{
  */ 

/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_Defines
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_Variables
  * @{
  */ 

USART_TypeDef* COM_USART[COMn] = {EVAL_COM1}; 

GPIO_TypeDef* COM_TX_PORT[COMn] = {EVAL_COM1_TX_GPIO_PORT};
 
GPIO_TypeDef* COM_RX_PORT[COMn] = {EVAL_COM1_RX_GPIO_PORT};

const uint32_t COM_USART_CLK[COMn] = {EVAL_COM1_CLK};

const uint32_t COM_TX_PORT_CLK[COMn] = {EVAL_COM1_TX_GPIO_CLK};
 
const uint32_t COM_RX_PORT_CLK[COMn] = {EVAL_COM1_RX_GPIO_CLK};

const uint16_t COM_TX_PIN[COMn] = {EVAL_COM1_TX_PIN};

const uint16_t COM_RX_PIN[COMn] = {EVAL_COM1_RX_PIN};

const uint8_t COM_TX_PIN_SOURCE[COMn] = {EVAL_COM1_TX_SOURCE};

const uint8_t COM_RX_PIN_SOURCE[COMn] = {EVAL_COM1_RX_SOURCE};
 
const uint8_t COM_TX_AF[COMn] = {EVAL_COM1_TX_AF};
 
const uint8_t COM_RX_AF[COMn] = {EVAL_COM1_RX_AF};


/**
  * @}
  */ 


/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_FunctionPrototypes
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup STM322xG_EVAL_LOW_LEVEL_Private_Functions
  * @{
  */

/**
  * @brief  Configures COM port.
  * @param  COM: Specifies the COM port to be configured.
  *   This parameter can be one of following parameters:    
  *     @arg COM1
  *     @arg COM2  
  * @param  USART_InitStruct: pointer to a USART_InitTypeDef structure that
  *   contains the configuration information for the specified USART peripheral.
  * @retval None
  */
void STM_EVAL_COMInit(COM_TypeDef COM, USART_InitTypeDef* USART_InitStruct)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable GPIO clock */
  RCC_AHB1PeriphClockCmd(COM_TX_PORT_CLK[COM] | COM_RX_PORT_CLK[COM], ENABLE);

  if (COM == COM1)
  {
    /* Enable UART clock */
    RCC_APB2PeriphClockCmd(COM_USART_CLK[COM], ENABLE);
  }

  /* Connect PXx to USARTx_Tx*/
  GPIO_PinAFConfig(COM_TX_PORT[COM], COM_TX_PIN_SOURCE[COM], COM_TX_AF[COM]);

  /* Connect PXx to USARTx_Rx*/
  GPIO_PinAFConfig(COM_RX_PORT[COM], COM_RX_PIN_SOURCE[COM], COM_RX_AF[COM]);

  /* Configure USART Tx as alternate function  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

  GPIO_InitStructure.GPIO_Pin = COM_TX_PIN[COM];
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(COM_TX_PORT[COM], &GPIO_InitStructure);

  /* Configure USART Rx as alternate function  */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = COM_RX_PIN[COM];
  GPIO_Init(COM_RX_PORT[COM], &GPIO_InitStructure);

  /* USART configuration */
  USART_Init(COM_USART[COM], USART_InitStruct);
    
  /* Enable USART */
  USART_Cmd(COM_USART[COM], ENABLE);
}

/**
  * @brief  Get Bootconfig information
  * @param  
  * @retval 
  */
int8_t GetBootConfig (stBootConfig * pBootCfg)
{
	pBootCfg->ActiveSect = *(__IO uint8_t*)ACTIVE_SECTION_ADDR;
	pBootCfg->InactiveSect = *(__IO uint8_t*)INACTIVE_SECTION_ADDR;
	pBootCfg->Sect1Valid = *(__IO uint8_t*)SECTION_1_VALID_ADDR;
	pBootCfg->Sect2Valid = *(__IO uint8_t*)SECTION_2_VALID_ADDR;
	pBootCfg->Sect1Ver = *(__IO uint32_t*)SECTION_1_VERSION_ADDR;
	pBootCfg->Sect2Ver = *(__IO uint32_t*)SECTION_2_VERSION_ADDR;
	if(pBootCfg->ActiveSect != SECTION_1 && pBootCfg->ActiveSect != SECTION_2)	return -1;
	if(pBootCfg->InactiveSect != SECTION_1 && pBootCfg->InactiveSect != SECTION_2)	return -1;
	if(pBootCfg->Sect1Valid != SECTION_VALID && pBootCfg->Sect1Valid != SECTION_INVALID)	return -1;
	if(pBootCfg->Sect2Valid != SECTION_VALID && pBootCfg->Sect2Valid != SECTION_INVALID)	return -1;
	return 0;
}

    
/*****END OF FILE****/
