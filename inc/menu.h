/**
  ******************************************************************************
  * @file    menu.h 
  * @author  Harris Chen
  * @version V0.0.1
  * @date    22-Sep-2017
  * @brief   This file provides all the headers of the menu functions.
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

/* Includes ------------------------------------------------------------------*/
#include "flash_if.h"

/* Private variables ---------------------------------------------------------*/
typedef  void (*pFunction)(void);

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Main_Menu(void);

#endif  /* __MENU_H */

/*****END OF FILE******/
