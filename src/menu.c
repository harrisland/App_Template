/**
  ******************************************************************************
  * @file    menu.c 
  * @author  Harris Chen
  * @version V0.0.1
  * @date    22-Sep-2017
  * @brief   This file provides the software which contains the main menu routine.
  *          The main menu gives the options of:
  *             - downloading a new binary file, 
  *             - uploading internal flash memory,
  *             - executing the binary file already loaded 
  *             - disabling the write protection of the Flash sectors where the 
  *               user loads his binary file.
  ******************************************************************************
  */ 

/** @addtogroup STM32F2xx_IAP
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "flash_if.h"
#include "menu.h"
#include "ymodem.h"
#include "C18_cfg.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
pFunction Jump_To_Application;
uint32_t JumpAddress;
__IO uint32_t FlashProtection = 0;
uint8_t tab_1024[1024] =
  {
    0
  };
uint8_t FileName[FILE_NAME_LENGTH];

/* Private function prototypes -----------------------------------------------*/
void SerialDownload(__IO uint32_t FlashAddress);
void SerialUpload(__IO uint32_t FlashAddress);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Download a file via serial port
  * @param  None
  * @retval None
  */
void SerialDownload(__IO uint32_t FlashAddress)
{
  uint8_t Number[10] = "          ";
  int32_t Size = 0;
	uint32_t CfgWord = 0;
	stBootConfig BootCfg={0};

  SerialPutString("Waiting for the file to be sent ... (press 'a' to abort)\n\r");
  Size = Ymodem_Receive(&tab_1024[0],FlashAddress);
	GetBootConfig(&BootCfg);
  if (Size > 0)
  {
		if(FlashAddress == ADDR_SECTION1)	CfgWord = SECTION_1 + (BootCfg.ActiveSect<<8) + (SECTION_VALID<<16) + (BootCfg.Sect2Valid<<24);
		else if(FlashAddress == ADDR_SECTION2)	CfgWord = SECTION_2 + (BootCfg.ActiveSect<<8) + (BootCfg.Sect1Valid<<16) + (SECTION_VALID<<16);
		FLASH_ProgramWord(ADDR_BOOTCFG,CfgWord);
		FLASH_ProgramWord(ADDR_BOOTCFG+4,0x00010000);
		FLASH_ProgramWord(ADDR_BOOTCFG+8,0x00010000);
    SerialPutString("\n\n\r Programming Completed Successfully!\n\r--------------------------------\r\n Name: ");
    SerialPutString(FileName);
    Int2Str(Number, Size);
    SerialPutString("\n\r Size: ");
    SerialPutString(Number);
    SerialPutString(" Bytes\r\n");
    SerialPutString("-------------------\n");
  }
  else if (Size == -1)
  {
		if(FlashAddress == ADDR_SECTION1)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (SECTION_INVALID<<16) + (BootCfg.Sect2Valid<<24);
		else if(FlashAddress == ADDR_SECTION2)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (BootCfg.Sect1Valid<<16) + (SECTION_INVALID<<16);
		FLASH_ProgramWord(ADDR_BOOTCFG,CfgWord);
		FLASH_ProgramWord(ADDR_BOOTCFG+4,0x00010000);
		FLASH_ProgramWord(ADDR_BOOTCFG+8,0x00010000);
    SerialPutString("\n\n\rThe image size is higher than the allowed space memory!\n\r");
  }
  else if (Size == -2)
  {
		if(FlashAddress == ADDR_SECTION1)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (SECTION_INVALID<<16) + (BootCfg.Sect2Valid<<24);
		else if(FlashAddress == ADDR_SECTION2)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (BootCfg.Sect1Valid<<16) + (SECTION_INVALID<<16);
		FLASH_ProgramWord(ADDR_BOOTCFG,CfgWord);
		FLASH_ProgramWord(ADDR_BOOTCFG+4,0x00010000);
		FLASH_ProgramWord(ADDR_BOOTCFG+8,0x00010000);
    SerialPutString("\n\n\rVerification failed!\n\r");
  }
  else if (Size == -3)
  {
		if(FlashAddress == ADDR_SECTION1)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (SECTION_INVALID<<16) + (BootCfg.Sect2Valid<<24);
		else if(FlashAddress == ADDR_SECTION2)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (BootCfg.Sect1Valid<<16) + (SECTION_INVALID<<16);
		FLASH_ProgramWord(ADDR_BOOTCFG,CfgWord);
		FLASH_ProgramWord(ADDR_BOOTCFG+4,0x00010000);
		FLASH_ProgramWord(ADDR_BOOTCFG+8,0x00010000);
    SerialPutString("\r\n\nAborted by user.\n\r");
  }
  else
  {
		if(FlashAddress == ADDR_SECTION1)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (SECTION_INVALID<<16) + (BootCfg.Sect2Valid<<24);
		else if(FlashAddress == ADDR_SECTION2)	CfgWord = BootCfg.ActiveSect + (BootCfg.InactiveSect<<8) + (BootCfg.Sect1Valid<<16) + (SECTION_INVALID<<16);
		FLASH_ProgramWord(ADDR_BOOTCFG,CfgWord);
		FLASH_ProgramWord(ADDR_BOOTCFG+4,0x00010000);
		FLASH_ProgramWord(ADDR_BOOTCFG+8,0x00010000);
    SerialPutString("\n\rFailed to receive the file!\n\r");
  }
}

/**
  * @brief  Upload a file via serial port.
  * @param  None
  * @retval None
  */
void SerialUpload(__IO uint32_t FlashAddress)
{
  uint8_t status = 0 ; 
	uint32_t UserFlashSize = 0;	
	
	UserFlashSize = GetUserFlashSize(FlashAddress);
  SerialPutString("\n\n\rSelect Receive File\n\r");
  if (GetKey() == CRC16)
  {
    /* Transmit the flash image through ymodem protocol */
    status = Ymodem_Transmit((uint8_t*)FlashAddress, (const uint8_t*)"UploadedFlashImage.bin", UserFlashSize);

    if (status != 0) 
    {
      SerialPutString("\n\rError Occurred while Transmitting File\n\r");
    }
    else
    {
      SerialPutString("\n\rFile uploaded successfully \n\r");
    }
  }
}

/**
  * @brief  Display the Main Menu on HyperTerminal
  * @param  None
  * @retval None
  */
void Main_Menu(void)
{
  uint8_t key = 0;
	uint8_t ackstr[4] = {0x01,0x4F,0x02,0x00};
	int8_t rv = 0;
	uint32_t DestAddr = 0;
	uint32_t CurrAddr = 0;
	stBootConfig BootCfg={0};
	
	SerialPutString("OK");
  SerialPutString("\r\n======================================================================");
  SerialPutString("\r\n=              (C) COPYRIGHT 2017 KeyDisp                            =");
  SerialPutString("\r\n=                                                                    =");
  SerialPutString("\r\n=     C18 In-Application Programming Application  (Version 1.0.0)    =");
  SerialPutString("\r\n=                                                                    =");
  SerialPutString("\r\n=                                   By Harris Chen	                  =");
  SerialPutString("\r\n======================================================================");
  SerialPutString("\r\n\r\n");
	
	rv = GetBootConfig(&BootCfg);
	if(rv == 0)
	{
		if(BootCfg.ActiveSect == SECTION_1)
		{
			CurrAddr = ADDR_SECTION1;
			DestAddr = ADDR_SECTION2;
		}
		else if(BootCfg.ActiveSect == SECTION_2)
		{
			CurrAddr = ADDR_SECTION2;
			DestAddr = ADDR_SECTION1;
		}
	}
	else
	{
		DestAddr = ADDR_BOOTCFG;
	}
  /* Test if any sector of Flash memory where user application will be loaded is write protected */
  if (DestAddr != 0 && FLASH_If_GetWriteProtectionStatus(&DestAddr) == 0)   
  {
    FlashProtection = 1;
  }
  else
  {
    FlashProtection = 0;
  }

  while (1)
  {
    SerialPutString("\r\n================== Main Menu ============================\r\n\n");
    SerialPutString("  Download Image To the STM32F2xx Internal Flash ------- 1\r\n\n");
    SerialPutString("  Upload Image From the STM32F2xx Internal Flash ------- 2\r\n\n");
    SerialPutString("  Execute The New Program ------------------------------ 3\r\n\n");

    if(FlashProtection != 0)
    {
      SerialPutString("  Disable the write protection ------------------------- 4\r\n\n");
    }

    SerialPutString("==========================================================\r\n\n");

    /* Receive key */
    key = GetKey();

		if (key == '?')
		{
			SerialPutString(ackstr);
		}
    else if (key == 0x31)
    {
      /* Download user application in the Flash */
      SerialDownload(DestAddr);
    }
    else if (key == 0x32)
    {
      /* Upload user application from the Flash */
      SerialUpload(CurrAddr);
    }
    else if (key == 0x33) /* execute the new program */
    {
      JumpAddress = *(__IO uint32_t*) (DestAddr + 4);
      /* Jump to user application */
      Jump_To_Application = (pFunction) JumpAddress;
      /* Initialize user application's Stack Pointer */
      __set_MSP(*(__IO uint32_t*) DestAddr);
      Jump_To_Application();
    }
    else if ((key == 0x34) && (FlashProtection == 1))
    {
      /* Disable the write protection */
      switch (FLASH_If_DisableWriteProtection(&DestAddr))
      {
        case 1:
        {
          SerialPutString("Write Protection disabled...\r\n");
          FlashProtection = 0;
          break;
        }
        case 2:
        {
          SerialPutString("Error: Flash write unprotection failed...\r\n");
          break;
        }
        default:
        {
        }
      }
    }
    else
    {
      if (FlashProtection == 0)
      {
        SerialPutString("Invalid Number ! ==> The number should be either 1, 2 or 3\r");
      }
      else
      {
        SerialPutString("Invalid Number ! ==> The number should be either 1, 2, 3 or 4\r");
      }
    }
  }
}

/**
  * @}
  */

/*****END OF FILE******/
